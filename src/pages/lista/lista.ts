import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

import { CadastroPage } from '../cadastro/cadastro';

/**
 * Generated class for the ListaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-lista',
  templateUrl: 'lista.html',
})
export class ListaPage {

  public lista: Observable<any[]>
  

  constructor(public navCtrl: NavController, public db: AngularFirestore) {
    console.log(this.lista);
    this.lista = db.collection('filmes').valueChanges();

  }

  public apagar(id: string): void{
    this.db.collection('filmes').doc(id).delete();
  }

  public cadastrar(): void{
    this.navCtrl.push(CadastroPage);
  }


}
