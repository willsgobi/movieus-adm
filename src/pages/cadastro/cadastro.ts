import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm, Form} from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';



/**
 * Generated class for the CadastroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-cadastro',
  templateUrl: 'cadastro.html',
})
export class CadastroPage {

  constructor(public navCtrl: NavController, public db: AngularFirestore) {
  }

  public salvar(form: NgForm):void {
    let nome = form.value.nome;
    let genero = form.value.nome;
    let descricao = form.value.nome;

    let filmes = {
      nome: nome,
      genero: genero,
      descricao: descricao
    };

    this.db.collection('filmes').add(filmes)
    .then((ref) => {
      let id = ref.id;
      this.db.collection('filmes').doc(id).update({id: id});
    })
  this.navCtrl.pop();
  
  }

  

}

