import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { CadastroPage } from '../pages/cadastro/cadastro';
import { ListaPage } from '../pages/lista/lista';


import { AngularFireModule } from '@angular/fire';
// import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

const config = {
  apiKey: "AIzaSyDRNQXjAMl8YEdsmMi-he1yA2MzuCGSgUo",
  authDomain: "projeto-movieus.firebaseapp.com",
  databaseURL: "https://projeto-movieus.firebaseio.com",
  projectId: "projeto-movieus",
  storageBucket: "projeto-movieus.appspot.com",
  messagingSenderId: "721101691084"
};

@NgModule({
  declarations: [
    MyApp,
    CadastroPage,
    ListaPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    // AngularFireAuth,
    AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CadastroPage,
    ListaPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
